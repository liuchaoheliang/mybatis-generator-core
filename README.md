<!-- xml 合并 ，java 合并 -->
<property name="xmlMergeable" value="true"></property>
<property name="javaMergeable" value="true"></property>




<!-- mapper 继承BaseMapper父类 -->
<plugin type="org.mybatis.generator.plugins.MapperSuperPlugin"/>
<!-- mapper 添加@Mapper注解 -->
<plugin type="org.mybatis.generator.plugins.MapperAnnotationPlugin"/>
<!-- model 实体添加@Builder注解 -->
<plugin type="org.mybatis.generator.plugins.ModelBuilderAnnotationPlugin"/>
<!-- model 实体添加@Data注解 -->
<plugin type="org.mybatis.generator.plugins.ModelDataAnnotationPlugin"/>